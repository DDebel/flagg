module.exports = {
  noSemi: false,
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: 'none',
  useTabs: false,
  printWidth: 100,
  tabWidth: 2,
};
