export const colors = {
  RED: '#FF0000',
  YELLOW: '#FFDF29',
  GREEN: '#1DB954',
  BLACK: '#000000'
};
